from django.contrib import admin

from publication.models import Publications, PublicationImages

class PublicationImagesInline(admin.TabularInline):
    model = PublicationImages

class PublicationsAdmin(admin.ModelAdmin):
    readonly_fields = ('date',)
    inlines = [
        PublicationImagesInline,
    ]

admin.site.register(Publications, PublicationsAdmin)


