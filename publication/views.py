from django.db.models import F
from django.shortcuts import render
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response


from publication.models import Publications, Chosen, Comments, Likes
from publication.permissions import IsCommentOwnerOrReadOnly, IsPublicationsOwnerOrReadOnly
from publication.serializers import PublicationsSerializer, CommentSerializer, LikeSerializer
from user.models import User

class LikesView(GenericAPIView):

    def get(self, request):
        user = request.user
        publication = Publications.objects.get(id=self.kwargs['pk'])




class User_has_chosen(APIView):

    def get(self, request, pk):
        user = User.objects.get(id=pk)
        chosen_pubs = Chosen.objects.values_list('publication__text', flat=True).filter(user=user)
        return Response(chosen_pubs)


class ChosenView(APIView):

    def get(self, request, pk):
        user = request.user
        publication = Publications.objects.get(id=pk)
        if Chosen.objects.filter(user=user, publication=publication).exists():
            Chosen.objects.filter(user=user, publication=publication).delete()
            return Response('removed from chosen', status=status.HTTP_201_CREATED)
        else:
            Chosen.objects.create(user=user, publication=publication)
            return Response('successfully chosen', status=status.HTTP_200_OK)


class PublicationsView(ModelViewSet):
    serializer_class = PublicationsSerializer
    queryset = Publications.objects.prefetch_related('post_images', 'post_comments').annotate(
        owner_nick_name=F('owner__username'),
        owner_avatar=F('owner__avatar')
    ).order_by('-date')
    lookup_field = 'pk'
    permission_classes = (IsPublicationsOwnerOrReadOnly, )

    def get_object(self):
        obj = Publications.objects.prefetch_related('post_images').annotate(
            owner_nick_name=F('owner__username'),
            owner_avatar=F('owner__avatar')
        ).get(id=self.kwargs['pk'])
        self.check_object_permissions(self.request, obj)
        return obj

class CommentView(ModelViewSet):
    serializer_class = CommentSerializer
    queryset = Comments.objects.all()
    lookup_field = 'pk'
    permission_classes = (IsCommentOwnerOrReadOnly, )





