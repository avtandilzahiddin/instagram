from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models

class MyUserManager(BaseUserManager):
    use_in_migrations = True
    def _create_user(self, username, password, **extra_fields):
        """Create and save a User with the given email and password."""
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user
    def create_user(self, username, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(username, password, **extra_fields)
    def create_superuser(self, username, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(username, password, **extra_fields)

class User(AbstractUser):
    phone = models.CharField(
        verbose_name = 'Номер телефона',
        max_length = 255,
    )
    site = models.URLField('Ссылка на сайт',)
    bio = models.TextField('Bio ')
    avatar = models.FileField('Аватарка', upload_to='user_avatar/')

    objects = MyUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELD = []

    def __str__(self):
        return f'{self.username} | {self.first_name} {self.last_name}'